# Package

version        = "0.2.0"
author         = "eqf0"
description    = "simple TLDR pages client"
license        = "MIT"
namedBin["mt"] = "tldr"


# Dependencies

requires "nim >= 1.7.1"
requires "zip >= 0.3.0 & < 1.0.0"
requires "argparse >= 3.0.0 & < 4.0.0"
requires "https://github.com/disruptek/cutelog >= 2.0.0 & < 3.0.0"
