import 
  std/[
    strutils,
    strformat,
    json,
    options,
    os,
  ],
  argparse,
  cutelog,
  mt/[
    cache,
    pretty,
    prettypager
  ]

const
  NimblePkgVersion {.strdefine.} = "" # changed by nimble during build
  specVersion = 1.5
  undefinedLang = "und" # https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes
  defaultLang = "en" # is there a better way to do this?
  banner = """

   ____ ___  / /_
  / __ `__ \/ __/  👽 mucho texto
 / / / / / / /_    another take on a tldr pages client
/_/ /_/ /_/\__/
"""

func newPageUrl(pageName: string): string =
  &"https://github.com/tldr-pages/tldr/issues/new?title=page%20request:%20{pageName}"

proc getAllPages(platform: string, cacheDir = defaultCacheDir): seq[string] =
  ## Parse index and walks throug all available commands, add the availables
  ## for the given platform
  let index = readFile(cacheDir / "index.json").parseJson()
  for command in index["commands"]:
    let cmds = getElems command["platform"]
    if (platform == "all" or %*"common" in cmds or %*platform in cmds):
      result.add(getStr command["name"])

proc listPages(platform: string, colored: bool) =
  let note = &"listing all commands for {platform}...\n"
  var 
    message = helpSty.styleStr note
    pages = listCmdSty.styleLines(getAllPages platform, colored)
  pagedEcho(message & pages)

proc extractLang(language: string, includeCountry = false): string =
  ## A locale has some of the following formats
  ##
  ## * `ll`
  ## * `ll_CC`
  ## * `ll_CC.encoding`
  ## * `ll_CC@variant`
  ##
  ## Where `ll` specifiec the language and `CC` the country. More information
  ## here:
  ## https://www.gnu.org/software/gettext/manual/html_node/Locale-Names.html
  ##
  ## Both language codes and country codes are always two characters long, so 
  ## we just take the first two chars to get the language.
  ##
  ## Some languages have different translations per country, so a flag to try to
  ## get the country is also provided.
  ##
  if includeCountry:
    language[0..4]
  else:
    language[0..1]

iterator envLangs(): string =
  ## Yields the possible languages in the following order
  ##
  ## 1. Check the value of LANG. If not set, then skip to step 5.
  ## 2. Extract the priority list from LANGUAGE. If not set, start with an empty
  ## priority list.
  ## 3. Append the value of LANG to the priority list.
  ## 4. Follow the priority list in order.
  ## 5. Fall back to English if none of the languages are available.
  ##
  ## Both LANG and LANGUAGE may contain the values C or POSIX, which will be 
  ## ignored.
  ##
  ## As some languages have different translations per country, after yielding
  ## a language, the language with country will try to be yielded
  ##
  let invalid = ["POSIX", "C"]
  if existsEnv "LANG":
    let lang = getEnv "LANG"
    if lang notin invalid:
      if existsEnv "LANGUAGE":
        let languages = getEnv "LANGUAGE"
        if languages notin invalid:
          for l in languages.split ':':
            yield extractLang l

      yield extractLang lang
      if lang.len > 4: # I hope this is correct enough...
        yield extractLang(lang, includeCountry = true)
  yield defaultLang

iterator languages(lang: string): string =
  ## If provided laguage is not undefined, will just return it.
  ## Otherwise, will search the env vars for a suitable language
  if lang != undefinedLang:
    yield lang
  else:
    for l in envLangs():
      yield l

type Platform {.pure.} = enum
  ## Available platforms
  common
  osx
  linux
  android
  sunos
  windows

iterator platforms(platform: string): string =
  ## Yields possible plataforms in the following order
  ##
  ## 1. Host Plataform (overridable)
  ## 2. Common platform
  ## 3. Windows
  ## 4. Osx
  ## 5. Linux
  ## 6. Android
  ## 7. Sunos
  ##
  ## The host platform (yielded first) won't be yielded a second time
  yield platform
  for pl in Platform:
    if $pl != platform:
      yield $pl

iterator pagePaths(
  language, platform: string
  ): tuple[path: string, lang: string, plat: string] =
  ## Yields every possible path that migh contain a valid page, based on the
  ## given language and platform.
  ##
  ## If not found there, the search will continue according to the
  ## specification. Using available env vars, first looking for different
  ## platforms and then for different languages
  const pagePrefix = "pages"
  for l in languages(language):
    for p in platforms(platform):
      if l != defaultLang:
        yield ((&"{pagePrefix}.{l}") / p, l, p)
      else:
        yield (pagePrefix / p, l, p)

proc findPage(root, language, platform, page: string): Option[string] =
  for (dir, l, p) in pagePaths(language, platform):
    let path = root / dir / page
    if path.fileExists:
      if p != $common and p != platform:
        warn &"Page for platform not found! Displaying page for {p}!"
        warn "Press enter to continue"
        discard stdin.readLine()
      elif language == undefinedLang and l != "LANG".getEnv().extractLang():
        warn &"Page for system language not found! Displaying page for {l}!"
        warn "Press enter to continue"
        discard stdin.readLine()
      return some path

proc processPage(pageName, language, platform: string, colored: bool) =
  if not defaultCacheDir.dirExists:
    createDir defaultCacheDir
    updatePages()

  let path = defaultCacheDir.findPage(language, platform, pageName)
  if path.isSome:
    var 
      lines = readFile(get path)
      page = styleMdText(lines, colored)
    pagedEcho page
  elif language == undefinedLang:
    notice "Page not found! Would you like to contribute?"
    notice "Visit ", newPageUrl pageName
  else:
    notice "Page not found! Perhaps try another language?"

proc main() =
  var logger = newCuteConsoleLogger()
  addHandler logger

  var p = newParser:
    help(banner)
    flag("-v", "--version", help = "print version")
    flag("-u", "--update", help = "force pages update")
    flag("-l", "--list", help = "list all available commands") 
    flag("-d", "--dry", help = "turn off string styling")
    option(
      "-L",
      "--language",
      # und is reasonable default to check when the argument is passed
      # some kind of None, but still a string
      default = some undefinedLang,
      help = "specify pages language"
    )
    option("-p", "--platform", default = some hostOs, help = "specify os")
    arg("pages", nargs = -1, help = "command to search for")

  try:
    let 
      args = p.parse commandLineParams()
      colored = not args.dry

    if args.version:
      echo helpSty.styleLines(banner, colored)
      echo helpSty.styleLines(
        &"client version: {NimblePkgVersion}; spec version: {specVersion}",
        colored
      )

    elif args.update:
      updatePages()
    elif args.list:
      listPages(args.platform, colored)
    else:
      if args.pages.len > 0:
        let pageName = args.pages.join("-") & ".md"
        processPage(pageName, args.language, args.platform, colored)
      else:
        echo(helpSty.styleLines(p.help, colored))

  except ShortCircuit as e:
    if e.flag == "argparse_help":
      echo(helpSty.styleStr p.help)
  except UsageError:
    warn getCurrentExceptionMsg()

when isMainModule:
  main()
