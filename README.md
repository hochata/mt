# Mucho Texto 👽

TLDR pages client written in Nim 👑.

## What is that?

Never ever forget the flags of `tar`! 🤯.

[TLDR pages](https://github.com/tldr-pages/tldr) are a simplified version of
man pages based on community examples.

Just run `tldr <command>` and you will become a cool kid 😼.

## Nice. How to use it?

Just run

```sh
nimble install https://codeberg.org/eqf0/mt
```

And you will have a freshly compiled `tldr` binary! assuming you have `Nim`
installed 🤷...

It has a nice help and a cute banner. You're welcome.

**WIP**

* [X] Implement [minimal spec](https://github.com/tldr-pages/tldr/blob/master/CLIENT-SPECIFICATION.md)
* [X] Pretty print (can always be prettier)
* [X] Add more interactivity: a progress bar, interactive options, etc
* [X] Prettier pager (thanks! [pager](https://git.sr.ht/~reesmichael1/nim-pager))
* [ ] Benchmark: Nim is fast, it would be fun to see how fast I can make it
* [ ] Add configuration options for colors, cache, auto update, etc

## Name
*Mucho texto* is spanish for *too much text*

Doesn't that remind of something? Whatever.
