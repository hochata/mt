import
  std/[
    httpclient,
    asyncdispatch,
    os,
    terminal,
    strutils
  ],
  zip/zipfiles,
  cutelog

const
  pagesUrl* = "https://tldr.sh/assets/tldr.zip "
  defaultCacheDir* = getCacheDir() / "tldr"
  progressStyle = {styleBright, styleItalic}

proc colorfulBar(total, progress, speed: BiggestInt) {.async.} =
  let p = int(progress.int * 100 / total.int)
  styledEcho progressStyle, $p, "% [", '='.repeat p, ' '.repeat(100 - p), "]"
  cursorUp 1
  eraseLine()

proc downloadPages(
  url = pagesUrl,
  cacheDir = defaultCacheDir
  ): Future[string] {.async.} =
  var c = newAsyncHttpClient()
  c.onProgressChanged = colorfulBar
  result = await c.getContent url

proc uncompressPages(compressedPages: string, cacheDir = defaultCacheDir) =
  var z: ZipArchive
  z.fromBuffer compressedPages
  z.extractAll cacheDir

proc updatePages*() =
  info "updating pages..."
  let pages = waitFor downloadPages()
  uncompressPages pages

